#!/bin/bash

TXT_FILE="$1"
MLF_OUT_FILE="$2"

TXT_FILE_LOWER="$TXT_FILE.lower"

#LM_TOOLS_PATH="/home/verbio/SREngines/processors/lm_tools/"
LM_TOOLS_PATH="../"

if [ "$TXT_FILE" == "" ] || [ ! -f $TXT_FILE ]; then
        echo "Invalid input txt file."
        exit -1
fi

if [ "$MLF_OUT_FILE" == "" ] ; then
        echo "Invalid output mlf file."
        exit -1
fi

dos2unix $TXT_FILE

$LM_TOOLS_PATH/tolower.sh $TXT_FILE > $TXT_FILE_LOWER

echo "** WARNING: NOT NORMALIZING TEXT ***"
#$LM_TOOLS_PATH/normalize_text.sh $TXT_FILE_LOWER $LM_TOOLS_PATH/confignopunct.txt

#for i in $(cat "$TXT_FILE_LOWER.norm"); do 
#        echo $i | grep -E "[0-9]" 
#done 

cp -v $TXT_FILE_LOWER "$TXT_FILE_LOWER.norm"

LAB_FILE=${TXT_FILE%.*}.lab

echo '#!MLF!#' > $MLF_OUT_FILE
echo "\"*/$LAB_FILE\"" >> $MLF_OUT_FILE

for i in $(cat "$TXT_FILE_LOWER.norm"); do 
        echo $i | sed 's/://g' | sed 's/#//g' | sed 's/\[//g' | sed 's/\]//g' | sed 's/?//g' | sed 's/�//g' | sed 's/!//g' | sed 's/�//g' | sed 's/\.//g' | sed 's/,//g' | sed 's/;//g' | sed 's/^\-//g' | sed 's/\-$//g' | sed 's/�/�/g' | sed 's/�/�/g' | sed 's/�/�/g' | sed 's/�/�/g' | sed 's/�/�/g' | sed 's/�/�/g' | tr '[:upper:]' '[:lower:]' | grep -v "^$" >> $MLF_OUT_FILE
done 

echo "." >> $MLF_OUT_FILE
